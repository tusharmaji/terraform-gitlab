import boto3 
import json 
s3_client = boto3.client('s3') 
dynamodb = boto3.resource('dynamodb') 
def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name'] 
    json_file_name = event['Records'][0]['s3']['object']['key']
    print(bucket)
    print(json_file_name)
    json_object = s3_client.get_object(Bucket=bucket,Key=json_file_name) 
    print("Object Finding")
    jsonFileReader = json_object['Body'].read()
    print("File Read") 
    jsonDict = json.loads(jsonFileReader)
    table = dynamodb.Table('dynamodb-test')
    print(table)
    print(jsonDict)
    table.put_item(Item=jsonDict)
    print("file put in dynamodb")
    return 'Hello from Lambda'